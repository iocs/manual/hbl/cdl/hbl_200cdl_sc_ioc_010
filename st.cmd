# Startup for HBL-200CDL:SC-IOC-010

# Load required modules
require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load PLC specific startup script
iocshLoad("$(IOCSH_TOP)/iocsh/hbl_200cdl_cryo_plc_010.iocsh", "DBDIR=$(IOCSH_TOP)/db/, MODVERSION=$(IOCVERSION=)")

